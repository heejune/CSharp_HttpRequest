﻿# KNOCK POS 

 **Knock POS** for  shop BarCode Payment System.


현재 서버쪽은 테스트를 위해 만들어 놓은 상태이며 서버 부분은 추후 Knock에서 제작하여야 함.
통신 및 서버 부분은 모두 프로젝트로 제작 되어 있으며 해당 프로젝트 명은 다음과 같다.
**http 통신 모듈**
> HttpRequest

**http 통신 테스트 서버 모듈**
> BarcodeExpress

  

# Requirements

## 바코드 정보 획득 

Knock UI 에서 바코드 정보를 입력 받는 부분 필요. 

  
## Knock 서버와 통신하는 통신 모듈
C# 에서 Http 모듈을 활용하여 서버와 통신하는 모듈 제작

  
# API DESC

## http 통신 API

|Method |Param |Param Type |Param Desc |
|:-:|:-:|:-:|:-:|
|Send | state | string | Post, Get 상태를 나타냄|
| - | strUrl | string | 서버 URL |
| - | strEntity | string | body 에 입력할 정보 |


