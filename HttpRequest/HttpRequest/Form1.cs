﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Printing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Newtonsoft.Json.Linq;

namespace HttpRequest
{
    public partial class Form1 : Form
    {
        Issue myParser = new Issue();

        public Form1()
        {
            InitializeComponent();
            this.KeyPreview = true;
            
        }

        private void BarCodeKeyPress(object sender, KeyPressEventArgs e)
        {
            Barcodelabel.Text += e.KeyChar;

            if (e.KeyChar == (char)Keys.Return)
                return;
        }

        private void payment_Click(object sender, EventArgs e)
        {
            httpRequest hpost = new httpRequest();

            string strBarCode = "Barcode=" + Barcodelabel.Text + "State = 1";

            // 결제 정보 획득 
            string result = hpost.Send("Post", "http://yangheejun.vps.phps.kr:7637/barcode/check", strBarCode);

            myParser.ParseJson(result);

            MessageBox.Show(result);
        }

        private void Print_Click(object sender, EventArgs e)
        {
            // 인쇄 시작후 진행상황 화면 출력시 보임...
            printPreviewDialog1.Document = printDocument1;
            printDocument1.PrintPage += new System.Drawing.Printing.PrintPageEventHandler(CreateReceipt);
            printPreviewDialog1.ShowDialog(); // 인쇄 시작 명령
        }

        private void CreateReceipt(object sender, PrintPageEventArgs e)
        {
            int count = myParser.issues.Count;
            int sum = 0;
            //this prints the reciept

            Graphics graphic = e.Graphics;

            Font font = new Font("Courier New", 8); //must use a mono spaced font as the spaces need to line up

            float fontHeight = font.GetHeight();

            int startX = 10;
            int startY = 10;
            int offset = 40;
            
            graphic.DrawString("".PadRight(5) + "영 수 증", new Font("Courier New", 18), new SolidBrush(Color.Black), startX, startY);
            string top = "상  호 : " + "에이브로스 카페";
            graphic.DrawString(top, font, new SolidBrush(Color.Black), startX, startY + offset);
            offset = offset + (int)fontHeight; //make the spacing consistent
            string top1 = "사업자번호: 123-12-12455  " + "대표자: 박해선";
            graphic.DrawString(top1, font, new SolidBrush(Color.Black), startX, startY + offset);
            offset = offset + (int)fontHeight; //make the spacing consistent
            string top2 = "주  소: 경기도 성남시 태평2동 4544번지";
            graphic.DrawString(top2, font, new SolidBrush(Color.Black), startX, startY + offset);
            offset = offset + (int)fontHeight; //make the spacing consistent
            string top3 = "전  화: 010-5570-0704";
            graphic.DrawString(top3, font, new SolidBrush(Color.Black), startX, startY + offset);


            offset = offset + (int)fontHeight; //make the spacing consistent
            graphic.DrawString("---------------------------------------", font, new SolidBrush(Color.Black), startX, startY + offset);
            offset = offset + 3; //make the spacing consistent
            graphic.DrawString("---------------------------------------", font, new SolidBrush(Color.Black), startX, startY + offset);
            offset = offset + (int)fontHeight + 5; //make the spacing consistent
            graphic.DrawString("메 뉴 명" + "단가".PadLeft(10) + "수량".PadLeft(5) + "금액".PadLeft(8) , font, new SolidBrush(Color.Black), startX, startY + offset);
            offset = offset + (int)fontHeight; //make the spacing consistent
            graphic.DrawString("---------------------------------------", font, new SolidBrush(Color.Black), startX, startY + offset);
            offset = offset + 3; //make the spacing consistent
            graphic.DrawString("---------------------------------------", font, new SolidBrush(Color.Black), startX, startY + offset);

            offset = offset + 30; //make some room so that the total stands out.
            for (int i = 0; i < count; i++)
            {
                int nPrice = Int32.Parse(myParser.issues[i].price);
                int nCount = Int32.Parse(myParser.issues[i].count);
                graphic.DrawString(String.Format("{0:c}", myParser.issues[i].name) + String.Format("{0:d}".PadLeft(13), myParser.issues[i].price) + String.Format("{0:d}".PadLeft(10), nCount) + String.Format("{0:d}".PadLeft(11), nPrice * nCount), font, new SolidBrush(Color.Black), startX, startY + offset);
                offset = offset + 15;
                sum += nPrice * nCount; 
            }

            offset = offset + (int)fontHeight; //make the spacing consistent
            graphic.DrawString("---------------------------------------", font, new SolidBrush(Color.Black), startX, startY + offset);
            offset = offset + (int)fontHeight+ 10; //make the spacing consistent
            graphic.DrawString("판매금액" + String.Format("{0:d}".PadLeft(16), sum) , new Font("Courier New", 12), new SolidBrush(Color.Black), startX, startY + offset);
            offset = offset + 30;
            graphic.DrawString("Knock pay 결재 영수증 입니다.", font, new SolidBrush(Color.Black), startX, startY + offset);
            offset = offset + (int)fontHeight; //make the spacing consistent
            graphic.DrawString("감사합니다.", font, new SolidBrush(Color.Black), startX, startY + offset);

        }
    }
}
