﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Newtonsoft.Json.Linq;

namespace HttpRequest
{
    class Issue
    {
        public string name { get; set; }
        public string count { get; set; }
        public string price { get; set; }
        public List<Issue> issues;

        public void ParseJson(String json)
        {
            issues = new List<Issue>();

            JObject obj = JObject.Parse(json);
            JArray array = JArray.Parse(obj["data"].ToString());
            foreach (JObject itemobj in array)
            {
                JArray menus = JArray.Parse(itemobj["Menus"].ToString());
                foreach (JObject menuobj in menus)
                {
                    Issue issue = new Issue();
                    issue.name = menuobj["name"].ToString();
                    issue.count = menuobj["count"].ToString();
                    issue.price = menuobj["price"].ToString();
                    issues.Add(issue);
                }
            }

        }
    }
}
