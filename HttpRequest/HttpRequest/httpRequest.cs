﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using System.IO;
using System.Net.Configuration;

namespace HttpRequest
{
    class httpRequest
    {
        public string Send(string state, string strUrl, string strEntity)
        {
            string result = "";
            string urlString = strUrl;

            HttpWebRequest request = null;
            HttpWebResponse response = null;

            try
            {
                Uri url = new Uri(urlString);
                request = (HttpWebRequest)WebRequest.Create(url);

                if (state.Equals("Post"))
                    request.Method = WebRequestMethods.Http.Post;
                else if (state.Equals("Get"))
                    request.Method = WebRequestMethods.Http.Get;
                else
                    return result = "state value Error!!";

                request.Timeout = 5000;

                // 인코딩 - UTF-8
                byte[] data = Encoding.UTF8.GetBytes(strEntity);
                request.ContentType = "application/x-www-form-urlencoded";
                request.ContentLength = data.Length;

                // 데이터 전송
                Stream dataStream = request.GetRequestStream();
                dataStream.Write(data, 0, data.Length);
                dataStream.Close();

                // 전송 응답
                response = (HttpWebResponse)request.GetResponse();
                Stream responseStream = response.GetResponseStream();
                StreamReader streamReader = new StreamReader(responseStream, Encoding.UTF8);
                result = streamReader.ReadToEnd();

                // 연결 닫기
                streamReader.Close();
                responseStream.Close();
                response.Close();
            }
            catch (Exception ex)
            {
                return result;
            }

            return result;
        }
    }
}
