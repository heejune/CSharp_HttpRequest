﻿const express = require('express');
const mongoose = require('mongoose');
const autoIncrement = require('mongoose-auto-increment');

mongoose.Promise = global.Promise;

var db = mongoose.createConnection('mongodb://localhost/barcode');

mongoose.plugin(schema => { schema.options.usePushEach = true });
db.on('error', console.error);
db.once('open', function () {
    console.log('MongoDB connected');
});
autoIncrement.initialize(db);

module.exports = db;