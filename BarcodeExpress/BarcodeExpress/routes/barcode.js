﻿'use strict';
var express = require('express');
var router = express.Router();
var barcode = require('../models/barcode');

router.post('/check', function (req, res) {
    var BarCode = req.body.Barcode;
    console.log(BarCode);

    if (BarCode == null) {
        res.json({ result: 2, msg: 'barcode is null!' });
    }
    else {
        barcode.find({ /*'barcode': BarCode*/ }, function (err, ret) {
            if (err) {
                res.json({ result: 2, msg: 'Barcode does not exist!' });
            }
            else {
                res.json({ result: 1, msg: 'Menus List', data: ret });
            }
        });
    }
});

router.post('/set', function (req, res) {
    var BarCode = new barcode();
    BarCode.barcode = '8801382124849';
    BarCode.Menus.name = '꼬치구이'; 
    BarCode.Menus.price = '5000';
    //BarCode.Menus.name = '소주';
    //BarCode.Menus.price = '3000';
    
    BarCode.save({}, function (err) {
            if (err) {
                res.json({ result: 2, msg: 'error', data:err });
            }
            else {
                res.json({ result: 1, msg: 'Menus save', data:BarCode});
            }
        });
});
module.exports = router;
