﻿const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const db = require('../lib/db');

const menuSchema = new Schema({
    name    : String,
    price   : String
})
const barcodeSchema = new Schema({
    barcode : String,
    Menus   : [{ type: menuSchema}]
});

module.exports = db.model('barcode', barcodeSchema);
